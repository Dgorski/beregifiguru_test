from pages.base_page import BasePage
from pages.locators import main_page as loc
from tests.test_data import data
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class MainPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.page_url = data.home_url

    def input_to_search(self, value):
        return self.find(loc.SEARCH_BOX).send_keys(value)

    def click_to_searching(self):
        return self.click(loc.SEARCH_BUTTON)

    def click_to_notification(self):
        return self.click(loc.NOTIFICATION_ICON)

    def click_to_notification_more(self):
        more = self.click(loc.NOTIFICATION_MORE)
        WebDriverWait(self.driver, 10).until(EC.text_to_be_present_in_element_attribute(loc.NOTIFICATION_MORE, 'style',
                                                                                        'display: none;'))
        return more

    def get_notification_content(self):
        return self.find_all(loc.NOTIFICATION_CONTENT)

    def get_searching_result(self):
        return self.find_all(loc.SEARCH_RESULT_CONTENT)

    def get_header_soc_ico(self):
        return self.find_all(loc.HEAD_SOCIAL_NETWORK_ICON)

    def header_ico_link_attr(self):
        return [i.get_attribute('href') for i in self.find_all(loc.HEAD_SOCIAL_NETWORK_ICON)]

    def get_footer_soc_ico(self):
        return self.find_all(loc.TAIL_SOCIAL_NETWORK_ICON)

    def footer_ico_link_attr(self):
        return [i.get_attribute('href') for i in self.find_all(loc.HEAD_SOCIAL_NETWORK_ICON)]
