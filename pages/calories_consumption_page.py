from pages.base_page import BasePage
from pages.locators import calories_consumption_page as loc
from tests.test_data import data


class CaloriesConsumptionPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.page_url = data.home_url

    def move_to_calories_consumption_page(self):
        return self.move_to_element(loc.CALORIES_PAGE, loc.CALORIES_CONSUMPTION_PAGE)

    def click_to_aerobic_fitness(self):
        self.click(loc.AEROBIC_FITNESS_LINK)

    def clik_to_hiit_training_info(self):
        self.click(loc.CALORIES_CONSUMPTION_HIIT_TRAINING_LINK)

    def add_weight_in_form(self, value):
        self.clear(loc.AEROBIC_FITNESS_WEIGHT)
        return self.add_to_form(loc.AEROBIC_FITNESS_WEIGHT, value)

    def add_time_in_form(self, value):
        self.clear(loc.AEROBIC_FITNESS_TIME)
        return self.add_to_form(loc.AEROBIC_FITNESS_TIME, value)

    def get_calories_consumption_for_hiit(self):
        return self.find(loc.CALORIES_CONSUMPTION_FOR_HIIT_TRAINING).text

    def get_times_from_hiit_table_info(self):
        return [float(i.get_attribute('data-value')) for i in self.find_all(loc.CALORIES_CONSUMPTION_HIIT_TABLLE_TIME)]

    def get_calories_from_hiit_table_info(self):
        return [float(i.text) for i in self.find_all(loc.CALORIES_CONSUMPTION_HIIT_TABLLE_CALORIES)]



