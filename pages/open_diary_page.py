from pages.base_page import BasePage
from pages.locators import open_diary_page as loc
from tests.test_data import data
from selenium.common import NoSuchElementException


class OpenDiaryPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.page_url = data.home_url

    def move_to_open_diary_page(self):
        return self.move_to_element(loc.PERSONAL_DIARY, loc.PERSONAL_OPEN_DIARY)

    def get_person_with_open_profile_for_friends(self):
        return self.find(loc.PERSON_WITH_OPEN_DIARY_FOR_FRIENDS)

    def get_person_with_open_profile_for_all(self):
        return self.find(loc.PERSON_WITH_OPEN_DIARY_FOR_ALL)

    def get_profile_link_for_friend_open_diary(self):
        return self.find(loc.PERSON_WITH_OPEN_DIARY_FOR_FRIENDS_PROFILE_LINK).get_attribute('href')

    def get_profile_link_for_all_open_diary(self):
        return self.find(loc.PERSON_WITH_OPEN_DIARY_FOR_ALL_PROFILE_LINK).get_attribute('href')

    def get_friends_is_opened_status(self):
        return [i.text for i in self.find_all(loc.PERSON_WITH_OPEN_DIARY_FOR_FRIENDS_IS_OPEN_STATUS)]

    def get_all_is_opened_status(self):
        return [i.text for i in self.find_all(loc.PERSON_WITH_OPEN_DIARY_FOR_ALL_IS_OPEN_STATUS)]

    def open_person_profile(self, link):
        self.driver.get(link)

    def food_diary_is_available(self):
        try:
            self.find(loc.FOOD_DIARY_LINK).is_enabled()
        except NoSuchElementException:
            return False
        return True

    def exercise_diary_is_available(self):
        try:
            self.find(loc.EXERCISE_DIARY_LINK).is_enabled()
        except NoSuchElementException:
            return False
        return True

    def diet_diary_is_available(self):
        try:
            self.find(loc.DIET_STATISTIC_LINK).is_enabled()
        except NoSuchElementException:
            return False
        return True
