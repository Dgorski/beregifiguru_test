from pages.base_page import BasePage
from pages.locators import message_feed_page as loc
from selenium.common import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from tests.test_data import data


class MsgFeedPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.page_url = data.home_url

    def move_to_msg_feed_page(self):
        return self.click(loc.MSG_FEED_PAGE)

    def move_to_friends_page(self):
        return self.move_to_element(loc.PERSONAL_DIARY, loc.PERSONAL_DIARY_FRIENDS)

    def open_user_profile_with_last_comment(self):
        return self.click(loc.MSG_FEED_PAGE_NEWEST_POST_USER)

    def get_user_name_with_last_comment(self):
        return self.find(loc.MSG_FEED_PAGE_NEWEST_POST_USER).text

    def remove_existing_friends(self):
        try:
            all_links_to_delete = self.find_all(loc.EXISTING_FRIENDS)
            for link in all_links_to_delete:
                link.click()
                WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(loc.SUBMIT_FOR_DELITING))
                self.click(loc.SUBMIT_FOR_DELITING)
                WebDriverWait(self.driver, 10).until(EC.invisibility_of_element(loc.NOTY_TEXT))
        except NoSuchElementException:
            pass

    def add_user_with_last_comment_to_favorite(self):
        self.click(loc.ADD_USER_TO_FAVORITE)
        WebDriverWait(self.driver, 10).until(EC.invisibility_of_element(loc.NOTY_TEXT))

    def count_user_in_favorite(self):
        return self.find(loc.EXISTING_FRIENDS_COUNT).text

