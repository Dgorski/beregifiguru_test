from selenium.webdriver.common.by import By

SIGN_IN = (By.LINK_TEXT, 'Войти')
EMAIL = (By.XPATH, '//*[@id="menuLarge"]//*[@id="txtEmail"]')
PASSWORD = (By.XPATH, '//*[@id="menuLarge"]//*[@id="txtPassword"]')
SUBMIT = (By.XPATH, '//*[@id="menuLarge"]//button[@class="defaultButton xx-button sf-key-2-l"]')
SIGN_OUT = (By.LINK_TEXT, 'Выйти')
