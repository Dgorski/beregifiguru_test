from selenium.webdriver.common.by import By

SIGN_IN = (By.LINK_TEXT, 'Войти')
EMAIL = (By.XPATH, '//*[@id="menuLarge"]//*[@id="txtEmail"]')
PASSWORD = (By.XPATH, '//*[@id="menuLarge"]//*[@id="txtPassword"]')
SUBMIT = (By.XPATH, '//*[@id="menuLarge"]//button[@class="defaultButton xx-button sf-key-2-l"]')
PERSONAL_DIARY_BOX = (By.XPATH, '//*[@id="menuLarge"]/li[2]/a')
ERROR_SIGNIN_MESSAGE = (By.CSS_SELECTOR, '.attention.text')
