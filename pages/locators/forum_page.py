from selenium.webdriver.common.by import By

FORUM_BUTTON = (By.XPATH, '//*[@id="menuLarge"]//*[@href="/Форум" and @class]')
FORUM_SEARCH_BOX = (By.XPATH, '//*[@id="Query"]')
FORUM_SEARCH_BUTTON = (By.XPATH, '//button[@class="xx-icon orange stacked sf-magnifier-l"]')
FORUM_RESULT_CONTENT = (By.TAG_NAME, 'h2')
