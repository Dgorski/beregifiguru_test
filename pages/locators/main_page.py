from selenium.webdriver.common.by import By

HEAD_SOCIAL_NETWORK_ICON = (By.XPATH, '//*[@id="header"]//*[@target and @href and @class]')
TAIL_SOCIAL_NETWORK_ICON = (By.XPATH, '//*[@id="footer"]//*[@target and @href and @class]')
SEARCH_BOX = (By.XPATH, '//input[@type="search"]')
SEARCH_BUTTON = (By.XPATH, '//*[@class="ya-site-form__search-input-layout-r"]//*')
SEARCH_RESULT_CONTENT = (By.XPATH, '//yass-h3[@class="b-serp-item__title"]')

NOTIFICATION_ICON = (By.CSS_SELECTOR, '.xx-icon.sf-bell-l.largeMenuOnly')
NOTIFICATION_MORE = (By.XPATH, '//a[@class="showMore"]')
NOTIFICATION_CONTENT = (By.XPATH, '//div[@data-notifid]')
