from selenium.webdriver.common.by import By

CALORIES_PAGE = (By.LINK_TEXT, 'Калорийность')
VITAMINS_CALCULATION = (By.XPATH, '//li/a[contains(text(), "Витамины в продуктах")]')
VITAMINS_SEARCH = (By.XPATH, '//*[@placeholder="Название продукта"]')
VITAMINS_MINERALS_ADD = (By.XPATH, '//*[@class="selectLabel filterSelect"]/select')
VITAMINS_SEARCH_RUN = (By.XPATH, '//button[@title="Найти"]')
VITAMINS_SEARCHING_RESULT = (By.XPATH, '//*[@id="productsContainter"]/tbody/tr/td[@class="title"]')
VITAMINS_SEARCHING_RESULT_BY_MG = (By.XPATH, '//td[@colspan="3"]//div[text()="Mg"]/preceding-sibling::*')
VITAMINS_SEARCHING_RESULT_BY_ALPHA = (By.XPATH, '//td[@colspan="3"]//div[text()="A"]/preceding-sibling::*')
VITAMINS_SEARCHING_RESULT_BY_ALCOHOL = (By.XPATH, '//td[@colspan="3"]//div[text()="Алк"]/preceding-sibling::*')
