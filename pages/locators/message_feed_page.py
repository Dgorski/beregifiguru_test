from selenium.webdriver.common.by import By

PERSONAL_DIARY = (By.XPATH, '//*[@id="menuLarge"]/li[2]/a')
MSG_FEED_PAGE = (By.XPATH, '//div[@class="lefth2 link"]/a')
PERSONAL_DIARY_FRIENDS = (By.XPATH, '//a[@title="Друзья"][2]')
MSG_FEED_PAGE_NEWEST_POST_USER = (By.XPATH, '//table[@class="methods topics"]//tbody/tr[1]//div[@class="prof user"]/a')
ADD_USER_TO_FAVORITE = (By.ID, 'requestFavorite')
EXISTING_FRIENDS = (By.XPATH, '//*[@class="friends-block"]//*[contains(text(),"Удалить")]')
SUBMIT_FOR_DELITING = (By.XPATH, '//*[@class="xx-button sf-check-1-l"]')
EXISTING_FRIENDS_COUNT = (By.XPATH, '//span[@data-bind="text: myFavCount"]')
NOTY_TEXT = (By.XPATH, '//span[@class="noty_text"]')
