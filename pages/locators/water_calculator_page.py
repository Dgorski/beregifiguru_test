from selenium.webdriver.common.by import By

BODY_CALCULATIONS = (By.XPATH, '//*[@id="menuLarge"]/li[1]/a')
WATER_CALCULATION = (By.LINK_TEXT, 'Количество воды')
WATER_CALCULATION_START = (By.XPATH, '//*[@id="calcFormId"]/button')

WATER_CALCULATION_FORM_SEX = (By.XPATH, '//*[@id="radioSex"]/label[2]')
WATER_CALCULATION_FORM_ACTIVITY = (By.XPATH, '//*[@id="Activity"]')
WATER_CALCULATION_FORM_CALORIES = (By.XPATH, '//*[@id="calories"]')
WATER_CALCULATION_FORM_AGE = (By.XPATH, '//*[@id="Age"]')
WATER_CALCULATION_FORM_WEIGHT = (By.XPATH, '//*[@id="Weight"]')
WATER_CALCULATION_RUN = (By.CSS_SELECTOR, '.defaultButton.xx-button.sf-calculator-1-l')
WATER_CALCULATION_RERUN = (By.CSS_SELECTOR, '.xx-button.sf-calculator-1-l.G')
WATER_CALCULATION_MEAN_RESULT = (By.XPATH, '//*[@id="resvalueWrap"]/div')
WATER_CALCULATION_MORE_RESULT = (By.CSS_SELECTOR, '.defaultButton.xx-button.sf-arrow-circle-27-l.show-more')
WATER_CALCULATION_MORE_RESULT_TABLE_CONTENT = (By.XPATH, '//*[@class="values s"]//td')
WATER_CALCULATION_ACTIVITY_UP = (By.CSS_SELECTOR, '.waterS.sport.wbg')
