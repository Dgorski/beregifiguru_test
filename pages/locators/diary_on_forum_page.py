from selenium.webdriver.common.by import By

PERSONAL_DIARY = (By.XPATH, '//a[contains(text(), "dgorski: Личный дневник")]')
PERSONAL_DIARY_ON_FORUM = (By.XPATH, '//li/a[contains(text(), "Мой дневник на форуме")]')
PERSONAL_DIARY_ON_FORUM_LIKE_UNLIKE = (By.XPATH, '//a[@data-postid="83490"][2]')
PERSONAL_DIARY_ON_FORUM_LIKE_COUNTER = (By.XPATH, '//a[@data-postid="83490"][2]//span')
NOTIFICATION_ICON = (By.CSS_SELECTOR, '.xx-icon.sf-bell-l.largeMenuOnly')
NEWEST_IN_NOTIFICATION = (By.XPATH, '//div[@data-notifid][1]//span')
