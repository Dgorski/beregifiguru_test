from selenium.webdriver.common.by import By

BODY_CALCULATIONS = (By.XPATH, '//*[@id="menuLarge"]/li[1]/a')
METABOLIC_AGE_CALCULATION = (By.XPATH, '//a[@title="Метаболический возраст"][2]')
METABOLIC_AGE_CALCULATION_START = (By.CSS_SELECTOR, '.defaultButton.xx-button.sf-calculator-1-l.calculate')

METABOLIC_AGE_CALCULATION_FORM_SEX = (By.XPATH, '//*[@id="radioSex"]/label[2]')
METABOLIC_AGE_CALCULATION_FORM_WEIGHT = (By.XPATH, '//*[@id="Weight"]')
METABOLIC_AGE_CALCULATION_FORM_HEIGHT = (By.XPATH, '//*[@id="Height"]')
METABOLIC_AGE_CALCULATION_FORM_AGE = (By.XPATH, '//*[@id="Age"]')
METABOLIC_AGE_CALCULATION_RUN = (By.CSS_SELECTOR, '.defaultButton.xx-button.sf-calculator-1-l')
METABOLIC_AGE_CALCULATION_RESULT = (By.XPATH, '//div[@class="resvalue"]')
AGES_FROM_METABOLIC_GRAPH = (By.CSS_SELECTOR, '.highcharts-data-labels.highcharts-tracker')


