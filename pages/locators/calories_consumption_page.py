from selenium.webdriver.common.by import By

CALORIES_PAGE = (By.LINK_TEXT, 'Калорийность')
CALORIES_CONSUMPTION_PAGE = (By.XPATH, '//li/a[contains(text(), "Расход калорий по категориям")]')
AEROBIC_FITNESS_LINK = (By.XPATH, '//*[@itemprop="url" and @title="Аэробика и фитнесс"]')
AEROBIC_FITNESS_WEIGHT = (By.ID, 'weight')
AEROBIC_FITNESS_TIME = (By.ID, 'amount')
CALORIES_CONSUMPTION_FOR_HIIT_TRAINING = (By.XPATH, '//*[@id="td1736"]/span')
CALORIES_CONSUMPTION_HIIT_TRAINING_LINK = (By.XPATH, '//*[@itemprop="url" and @title="HIIT: интервальный тренинг"]')
CALORIES_CONSUMPTION_HIIT_TABLLE_TIME = (By.XPATH, '//*[@class="tableE"]//span[@data-value]')
CALORIES_CONSUMPTION_HIIT_TABLLE_CALORIES = (By.XPATH, '//*[@class="tableE"]//span[@data-bind]')