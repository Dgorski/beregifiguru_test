from selenium.webdriver.common.by import By

PERSONAL_DIARY = (By.XPATH, '//*[@id="menuLarge"]/li[2]/a')
PERSONAL_OPEN_DIARY = (By.XPATH, '//li[@rel="sharedProfiles_d"]//*[@title][2]')

PERSON_WITH_OPEN_DIARY_FOR_FRIENDS = (By.XPATH, '//div[@class="friends-block"][1]')
PERSON_WITH_OPEN_DIARY_FOR_FRIENDS_PROFILE_LINK = (By.XPATH, '//div[@class="friends-block"][1]/div/a[@data-bind="attr: { href: profileLink }"]')
PERSON_WITH_OPEN_DIARY_FOR_FRIENDS_IS_OPEN_STATUS = (By.XPATH, '//*[@class="friends-block"][1]//*[@class="share-denied"]')

PERSON_WITH_OPEN_DIARY_FOR_ALL = (By.XPATH, '//div[@class="friends-block"][5]')
PERSON_WITH_OPEN_DIARY_FOR_ALL_PROFILE_LINK = (By.XPATH, '//div[@class="friends-block"][5]/div/a[@data-bind="attr: { href: profileLink }"]')
PERSON_WITH_OPEN_DIARY_FOR_ALL_IS_OPEN_STATUS = (By.XPATH, '//*[@class="friends-block"][5]//*[@class="share-allow"]')

FOOD_DIARY_LINK = (By.XPATH, '//a[contains(text(), " Дневник питания")]')
EXERCISE_DIARY_LINK = (By.XPATH, '//a[contains(text(), " Дневник упражнений")]')
DIET_STATISTIC_LINK = (By.XPATH, '//a[contains(text(), " Статистика рациона")]')
