from selenium.webdriver.common.by import By

PROFILE_BANNER = (By.XPATH, '//div[@class="lefth2"]/a[@title and @href]')
PROFILE_NAME = (By.XPATH, '//*[@id="NickName"]')
PROFILE_EMAIL = (By.XPATH, '//*[@id="Email"]')
PROFILE_BIRTHDAY = (By.XPATH, '//*[@id="Birthday"]')
PROFILE_SUBMIT = (By.XPATH, '//*[@type="submit"]')
PROFILE_AVATAR = (By.XPATH, '//input[@type="file"]')
PROFILE_CURRENT_AVATAR = (By.XPATH, '//img[@alt="Аватар"]')
PROFILE_NOTIFY_MSG = (By.XPATH, '//span[@class="noty_text"]')
PROFILE_SET = [PROFILE_NAME, PROFILE_BIRTHDAY, PROFILE_EMAIL]

