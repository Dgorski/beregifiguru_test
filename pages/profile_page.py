from pages.base_page import BasePage
from pages.locators import profile_page as loc
from tests.test_data import data
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class ProfilePage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.page_url = data.home_url

    def click_to_profile_form(self):
        return self.click(loc.PROFILE_BANNER)

    def add_avatar(self, value):
        return self.find(loc.PROFILE_AVATAR).send_keys(value)

    def get_notify_message(self):
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(loc.PROFILE_NOTIFY_MSG))
        return self.find(loc.PROFILE_NOTIFY_MSG).text

    def submit_changes(self):
        return self.click(loc.PROFILE_SUBMIT)

    def is_avatar_posted(self):
        return self.find(loc.PROFILE_CURRENT_AVATAR)

    def get_some_profile_items(self):
        current_items = list()
        for locator in (loc.PROFILE_NAME, loc.PROFILE_BIRTHDAY, loc.PROFILE_EMAIL):
            current_items.append(self.find(locator).get_attribute('value'))
        return current_items

    def update_some_profile_items(self):
        for k, v in dict(zip(loc.PROFILE_SET, data.modify_profile_items)).items():
            self.clear(k)
            self.find(k).send_keys(v)
