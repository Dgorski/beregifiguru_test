from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from pages.locators import base_page as loc
from tests.test_data import data


class BasePage:

    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.page_url = ''

    def open(self):
        return self.driver.get(self.page_url)

    def sign_in(self, login, password):
        self.click(loc.SIGN_IN)
        self.add_to_form(loc.EMAIL, login)
        self.add_to_form(loc.PASSWORD, password)
        self.click(loc.SUBMIT)
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(loc.SIGN_OUT))

    def sign_out(self):
        self.click(loc.SIGN_OUT)

    def find(self, locator):
        return self.driver.find_element(*locator)

    def find_all(self, locator):
        return self.driver.find_elements(*locator)

    def clear(self, locator):
        return self.find(locator).clear()

    def click(self, locator):
        return self.find(locator).click()

    def get_text(self, locator):
        return self.find(locator).text

    def get_cookies(self):
        return self.driver.get_cookies()

    def move_to_element(self, starting_locator, ending_locator):
        starting_obj = self.find(starting_locator)
        ending_obj = self.find(ending_locator)
        action = ActionChains(self.driver)
        action.move_to_element(starting_obj)
        action.click(starting_obj)
        action.move_to_element(ending_obj)
        action.click(ending_obj)
        action.perform()

    def add_to_form(self, locator, value):
        self.clear(locator)
        return self.find(locator).send_keys(str(value))
