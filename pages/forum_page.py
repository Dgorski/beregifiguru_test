from pages.base_page import BasePage
from pages.locators import forum_page as loc
from tests.test_data import data


class ForumPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.page_url = data.home_url

    def click_to_forum(self):
        return self.click(loc.FORUM_BUTTON)

    def input_to_search(self, value):
        return self.find(loc.FORUM_SEARCH_BOX).send_keys(value)

    def click_to_searching(self):
        return self.click(loc.FORUM_SEARCH_BUTTON)

    def get_title_value_txt(self):
        return self.find(loc.FORUM_RESULT_CONTENT).text
