from selenium.webdriver.support.ui import Select

from pages.base_page import BasePage
from pages.locators import water_calculator_page as loc
from tests.test_data import data


class WaterCalculatorPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.page_url = data.home_url

    def move_to_water_calculation_page(self):
        return self.move_to_element(loc.BODY_CALCULATIONS, loc.WATER_CALCULATION)

    def click_start_calculation(self):
        return self.click(loc.WATER_CALCULATION_START)

    def add_sex_in_form(self):
        return self.click(loc.WATER_CALCULATION_FORM_SEX)

    def add_activity_in_form(self, txt):
        activity = self.find(loc.WATER_CALCULATION_FORM_ACTIVITY)
        choose_activity = Select(activity)
        choose_activity.select_by_visible_text(txt)

    def add_calories_in_form(self, value):
        return self.add_to_form(loc.WATER_CALCULATION_FORM_CALORIES, value)

    def add_age_in_form(self, value):
        return self.add_to_form(loc.WATER_CALCULATION_FORM_AGE, value)

    def add_weight_in_form(self, value):
        self.add_to_form(loc.WATER_CALCULATION_FORM_WEIGHT, value)

    def click_calculate(self):
        self.click(loc.WATER_CALCULATION_RUN)

    def click_recalculate(self):
        self.click(loc.WATER_CALCULATION_RERUN)

    def get_mean_result(self):
        return self.find(loc.WATER_CALCULATION_MEAN_RESULT).text

    def activity_up_by_sport(self):
        return self.click(loc.WATER_CALCULATION_ACTIVITY_UP)

    def get_min_cups_from_graph(self):
        return self.find_all(loc.WATER_CALCULATION_MORE_RESULT_TABLE_CONTENT)[4].text.split(' ')[0]

    def get_max_cups_from_graph(self):
        return self.find_all(loc.WATER_CALCULATION_MORE_RESULT_TABLE_CONTENT)[0].text.split(' ')[0]
