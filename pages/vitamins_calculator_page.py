from selenium.webdriver.support.ui import Select
from pages.base_page import BasePage
from pages.locators import vitamins_calculator_page as loc
from tests.test_data import data


class VitaminsPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.page_url = data.home_url

    def move_to_vitamins_page(self):
        return self.move_to_element(loc.CALORIES_PAGE, loc.VITAMINS_CALCULATION)

    def add_product(self, product_name):
        return self.add_to_form(loc.VITAMINS_SEARCH, product_name)

    def add_mineral_vitamin(self, txt):
        set_mineral_vitamins = self.find(loc.VITAMINS_MINERALS_ADD)
        mineral_vitamins = Select(set_mineral_vitamins)
        mineral_vitamins.select_by_value(txt)

    def click_calculate(self):
        self.click(loc.VITAMINS_SEARCH_RUN)

    def get_result_len(self):
        return len(self.find_all(loc.VITAMINS_SEARCHING_RESULT))

    def get_results_by_magnesium(self):
        magnesium_counter = []
        for i in self.find_all(loc.VITAMINS_SEARCHING_RESULT_BY_MG):
            if float(i.text.replace('%', '')) > 0:
                magnesium_counter.append(i)
        return len(magnesium_counter)

    def get_results_by_alpha(self):
        alpha_counter = []
        for i in self.find_all(loc.VITAMINS_SEARCHING_RESULT_BY_ALPHA):
            if float(i.text.replace('%', '')) > 0:
                alpha_counter.append(i)
        return len(alpha_counter)

    def get_results_by_alcohol(self):
        alcohol_counter = []
        for i in self.find_all(loc.VITAMINS_SEARCHING_RESULT_BY_ALCOHOL):
            if i.text != '-':
                if float(i.text.replace('%', '')) > 0:
                    alcohol_counter.append(i)
        return len(alcohol_counter)
