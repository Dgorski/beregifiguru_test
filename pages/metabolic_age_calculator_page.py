from pages.base_page import BasePage
from pages.locators import metabolic_age_calculator_page as loc
from tests.test_data import data


class MetabolicAgeCalculatorPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.page_url = data.home_url

    def move_to_metabolic_age_calculation_page(self):
        return self.move_to_element(loc.BODY_CALCULATIONS, loc.METABOLIC_AGE_CALCULATION)

    def click_start_calculation(self):
        return self.click(loc.METABOLIC_AGE_CALCULATION_START)

    def add_sex_in_form(self):
        return self.click(loc.METABOLIC_AGE_CALCULATION_FORM_SEX)

    def add_weight_in_form(self, value):
        return self.add_to_form(loc.METABOLIC_AGE_CALCULATION_FORM_WEIGHT, value)

    def add_height_in_form(self, value):
        return self.add_to_form(loc.METABOLIC_AGE_CALCULATION_FORM_HEIGHT, value)

    def add_age_in_form(self, value):
        return self.add_to_form(loc.METABOLIC_AGE_CALCULATION_FORM_AGE, value)

    def click_calculate(self):
        self.click(loc.METABOLIC_AGE_CALCULATION_RUN)

    def get_mean_result(self):
        return self.find(loc.METABOLIC_AGE_CALCULATION_RESULT).text

    def get_input_age_from_graph(self):
        return self.find_all(loc.AGES_FROM_METABOLIC_GRAPH)[0].text

    def get_metabolic_age_from_graph(self):
        return self.find_all(loc.AGES_FROM_METABOLIC_GRAPH)[1].text
