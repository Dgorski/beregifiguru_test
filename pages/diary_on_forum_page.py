from pages.base_page import BasePage
from pages.locators import diary_on_forum_page as loc
from tests.test_data import data


class DiaryForumPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.page_url = data.home_url

    def move_to_diary_forum_page(self):
        return self.move_to_element(loc.PERSONAL_DIARY, loc.PERSONAL_DIARY_ON_FORUM)

    def add_like_for_my_post(self):
        return self.click(loc.PERSONAL_DIARY_ON_FORUM_LIKE_UNLIKE)

    def remove_like_for_my_post(self):
        return self.add_like_for_my_post()

    def clic_to_notification(self):
        return self.click(loc.NOTIFICATION_ICON)

    def get_newest_msg_from_notification(self):
        return self.find(loc.NEWEST_IN_NOTIFICATION).text

    def if_post_has_like(self):
        value = self.find(loc.PERSONAL_DIARY_ON_FORUM_LIKE_COUNTER).text
        if int(value) > 0:
            return True
        return False
