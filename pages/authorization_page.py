from pages.base_page import BasePage
from pages.locators import authorization_page as loc
from tests.test_data import data
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class AuthorizationPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.page_url = data.home_url

    def sign_in(self, login, password):
        self.click(loc.SIGN_IN)
        self.add_to_form(loc.EMAIL, login)
        self.add_to_form(loc.PASSWORD, password)
        self.click(loc.SUBMIT)

    def signin_signup_status_txt(self):
        return self.find(loc.SIGN_IN).text

    def personal_diary_txt(self):
        WebDriverWait(self.driver, 10).until(EC.text_to_be_present_in_element(loc.PERSONAL_DIARY_BOX,
                                                                              data.valid_login.split('@')[0]))
        return self.find(loc.PERSONAL_DIARY_BOX).text

    def error_message(self):
        return self.find_all(loc.ERROR_SIGNIN_MESSAGE)[1]
