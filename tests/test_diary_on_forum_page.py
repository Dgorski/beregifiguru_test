import allure

from pages.diary_on_forum_page import DiaryForumPage
from tests.test_data import data


@allure.feature('Check_diary_forum_page')
@allure.story('Check_remove_like_to_post')
def test_remove_like_to_my_post(driver):
    with allure.step('Open_main_page'):
        diary_on_forum_page = DiaryForumPage(driver)
        diary_on_forum_page.open()
    with allure.step('Sign_In'):
        diary_on_forum_page.sign_in(data.valid_login, data.valid_password)
    with allure.step('Move_to_diary_forum_page'):
        diary_on_forum_page.move_to_diary_forum_page()
    with allure.step('Getting_auxiliary_data'):
        if diary_on_forum_page.if_post_has_like():
            diary_on_forum_page.remove_like_for_my_post()
    with allure.step('Validate'):
        assert not diary_on_forum_page.if_post_has_like()


@allure.feature('Check_diary_forum_page')
@allure.story('Check_add_like_to_post')
def test_add_like_to_my_post(driver):
    with allure.step('Open_main_page'):
        diary_on_forum_page = DiaryForumPage(driver)
        diary_on_forum_page.open()
    with allure.step('Sign_In'):
        diary_on_forum_page.sign_in(data.valid_login, data.valid_password)
    with allure.step('Move_to_diary_forum_page'):
        diary_on_forum_page.move_to_diary_forum_page()
    with allure.step('Getting_auxiliary_data'):
        if diary_on_forum_page.if_post_has_like():
            diary_on_forum_page.remove_like_for_my_post()
        diary_on_forum_page.add_like_for_my_post()
    with allure.step('Validate'):
        assert diary_on_forum_page.if_post_has_like()


@allure.feature('Check_diary_forum_page')
@allure.story('Check_note_in_notification')
def test_new_note_in_notification(driver):
    with allure.step('Open_main_page'):
        diary_on_forum_page = DiaryForumPage(driver)
        diary_on_forum_page.open()
    with allure.step('Sign_In'):
        diary_on_forum_page.sign_in(data.valid_login, data.valid_password)
    with allure.step('Move_to_diary_forum_page'):
        diary_on_forum_page.move_to_diary_forum_page()
    with allure.step('Getting_auxiliary_data'):
        if diary_on_forum_page.if_post_has_like():
            diary_on_forum_page.remove_like_for_my_post()
        diary_on_forum_page.add_like_for_my_post()
        diary_on_forum_page.clic_to_notification()
    with allure.step('Validate'):
        assert diary_on_forum_page.get_newest_msg_from_notification() == data.response
