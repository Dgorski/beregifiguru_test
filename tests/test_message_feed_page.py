import allure

from pages.message_feed_page import MsgFeedPage
from tests.test_data import data


@allure.feature('Check_message_feed_page')
@allure.story('Remove_users_from_favorite_list')
def test_remove_users_from_favorite_list(driver):
    with allure.step('Open_main_page'):
        msg_feed_page = MsgFeedPage(driver)
        msg_feed_page.open()
    with allure.step('Sign_In'):
        msg_feed_page.sign_in(data.valid_login, data.valid_password)
    with allure.step('Move_to_friends_page'):
        msg_feed_page.move_to_friends_page()
    with allure.step('Getting_auxiliary_data'):
        msg_feed_page.remove_existing_friends()
    with allure.step('Validate'):
        assert int(msg_feed_page.count_user_in_favorite()) == 0


@allure.feature('Check_message_feed_page')
@allure.story('Add_users_from_favorite_list')
def test_add_user_to_favorite_list(driver):
    with allure.step('Open_main_page'):
        msg_feed_page = MsgFeedPage(driver)
        msg_feed_page.open()
    with allure.step('Move_to_msg_feed_page'):
        msg_feed_page.move_to_msg_feed_page()
    with allure.step('Getting_auxiliary_data'):
        msg_feed_page.get_user_name_with_last_comment()
        msg_feed_page.open_user_profile_with_last_comment()
    with allure.step('Sign_In'):
        msg_feed_page.sign_in(data.valid_login, data.valid_password)
    with allure.step('Getting_auxiliary_data'):
        msg_feed_page.add_user_with_last_comment_to_favorite()
    with allure.step('Move_to_friends_page'):
        msg_feed_page.move_to_friends_page()
    assert int(msg_feed_page.count_user_in_favorite()) > 0
