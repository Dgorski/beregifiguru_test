import allure

from pages.open_diary_page import OpenDiaryPage


@allure.feature('Check_open_diary_page')
@allure.story('Check_compare_closed_diary_with_profile_page')
def test_compare_closed_diary_with_profile_page(driver):
    with allure.step('Open_main_page'):
        personal_open_diary_page = OpenDiaryPage(driver)
        personal_open_diary_page.open()
    with allure.step('Move_to_open_diary_page'):
        personal_open_diary_page.move_to_open_diary_page()
    with allure.step('Getting_auxiliary_data'):
        personal_open_diary_page.get_person_with_open_profile_for_friends()
        person_status = personal_open_diary_page.get_friends_is_opened_status()
        valid_status = {
            'друзьям': {
                'food_diary': False,
                'exersize_diary': False,
                'diet_diary': False
            },
            'всем': {
                'food_diary': True,
                'exersize_diary': True,
                'diet_diary': True
            }
        }
        profile_link = personal_open_diary_page.get_profile_link_for_friend_open_diary()
        personal_open_diary_page.open_person_profile(profile_link)
    with allure.step('Validate'):
        assert valid_status.get(person_status[0]).get(
            'food_diary') == personal_open_diary_page.food_diary_is_available()
        assert valid_status.get(person_status[1]).get(
            'exersize_diary') == personal_open_diary_page.exercise_diary_is_available()
        assert valid_status.get(person_status[2]).get(
            'diet_diary') == personal_open_diary_page.diet_diary_is_available()


@allure.feature('Check_open_diary_page')
@allure.story('Check_compare_opened_diary_with_profile_page')
def test_compare_opened_diary_with_profile_page(driver):
    with allure.step('Open_main_page'):
        personal_open_diary_page = OpenDiaryPage(driver)
        personal_open_diary_page.open()
    with allure.step('Move_to_open_diary_page'):
        personal_open_diary_page.move_to_open_diary_page()
    with allure.step('Getting_auxiliary_data'):
        personal_open_diary_page.get_person_with_open_profile_for_all()
        person_status = personal_open_diary_page.get_all_is_opened_status()
        valid_status = {
            'друзьям': {
                'food_diary': False,
                'exersize_diary': False,
                'diet_diary': False
            },
            'всем': {
                'food_diary': True,
                'exersize_diary': True,
                'diet_diary': True
            }
        }
        profile_link = personal_open_diary_page.get_profile_link_for_all_open_diary()
        personal_open_diary_page.open_person_profile(profile_link)
    with allure.step('Validate'):
        assert valid_status.get(person_status[0]).get(
            'food_diary') == personal_open_diary_page.food_diary_is_available()
        assert valid_status.get(person_status[1]).get(
            'exersize_diary') == personal_open_diary_page.exercise_diary_is_available()
        assert valid_status.get(person_status[2]).get(
            'diet_diary') == personal_open_diary_page.diet_diary_is_available()
