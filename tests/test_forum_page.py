import allure

from pages.forum_page import ForumPage
from tests.test_data import data


@allure.feature('Check_main_forum_page')
@allure.story('Check_searching_not_working')
def test_search_not_working_and_crashed_page(driver):
    with allure.step('Open_main_page'):
        forum_page = ForumPage(driver)
        forum_page.open()
    with allure.step('Move_to_main_forum_page'):
        forum_page.click_to_forum()
    with allure.step('Getting_auxiliary_data'):
        forum_page.input_to_search(data.searching_word)
        forum_page.click_to_searching()
    with allure.step('Validate'):
        assert forum_page.get_title_value_txt().find('ошибка')
