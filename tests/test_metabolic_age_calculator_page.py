import allure

from pages.metabolic_age_calculator_page import MetabolicAgeCalculatorPage


@allure.feature('Check_metabolic_calculator_page')
@allure.story('Check_metabolic_age_calculator_working')
def test_metabolic_age_calculator_working(driver):
    with allure.step('Open_main_page'):
        metabolic_age_page = MetabolicAgeCalculatorPage(driver)
        metabolic_age_page.open()
    with allure.step('Move_to_metabolic_calculator_page'):
        metabolic_age_page.move_to_metabolic_age_calculation_page()
    with allure.step('Getting_auxiliary_data'):
        metabolic_age_page.click_start_calculation()
        metabolic_age_page.add_sex_in_form()
        metabolic_age_page.add_weight_in_form(85)
        metabolic_age_page.add_height_in_form(180)
        input_age = 40
        metabolic_age_page.add_age_in_form(input_age)
        metabolic_age_page.click_calculate()
        metabolic_age = float(metabolic_age_page.get_mean_result())

        input_ages_from_graph = float(metabolic_age_page.get_input_age_from_graph())
        metabolic_age_from_graph = float(metabolic_age_page.get_metabolic_age_from_graph())
    with allure.step('Validate'):
        assert input_age == input_ages_from_graph and metabolic_age == metabolic_age_from_graph
