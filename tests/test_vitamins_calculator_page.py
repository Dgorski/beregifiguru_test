import allure

from pages.vitamins_calculator_page import VitaminsPage


@allure.feature('Check_vitamin_calculator_page')
@allure.story('Check_all_curd_products_contain_magnesium')
def test_all_curd_products_contain_magnesium(driver):
    with allure.step('Open_main_page'):
        vitamin_calculator_page = VitaminsPage(driver)
        vitamin_calculator_page.open()
    with allure.step('Move_to_vitamin_calculator_page'):
        vitamin_calculator_page.move_to_vitamins_page()
    with allure.step('Getting_auxiliary_data'):
        vitamin_calculator_page.add_product('творог')
        vitamin_calculator_page.add_mineral_vitamin('Mg')
        vitamin_calculator_page.click_calculate()
        result = vitamin_calculator_page.get_result_len()
    with allure.step('Validate'):
        assert result == vitamin_calculator_page.get_results_by_magnesium()


@allure.feature('Check_vitamin_calculator_page')
@allure.story('Check_any_curd_products_contain_alpha')
def test_any_curd_products_contain_alpha(driver):
    with allure.step('Open_main_page'):
        vitamin_calculator_page = VitaminsPage(driver)
        vitamin_calculator_page.open()
    with allure.step('Move_to_vitamin_calculator_page'):
        vitamin_calculator_page.move_to_vitamins_page()
    with allure.step('Getting_auxiliary_data'):
        vitamin_calculator_page.add_product('творог')
        vitamin_calculator_page.add_mineral_vitamin('Mg')
        vitamin_calculator_page.click_calculate()
        result = vitamin_calculator_page.get_result_len()
    with allure.step('Validate'):
        assert result != vitamin_calculator_page.get_results_by_alpha()


@allure.feature('Check_vitamin_calculator_page')
@allure.story('Check_all_curd_products_not_contain_alcohol')
def test_all_curd_products_not_contain_alcohol(driver):
    with allure.step('Open_main_page'):
        vitamin_calculator_page = VitaminsPage(driver)
        vitamin_calculator_page.open()
    with allure.step('Move_to_vitamin_calculator_page'):
        vitamin_calculator_page.move_to_vitamins_page()
    with allure.step('Getting_auxiliary_data'):
        vitamin_calculator_page.add_product('творог')
        vitamin_calculator_page.add_mineral_vitamin('Mg')
        vitamin_calculator_page.click_calculate()
    with allure.step('Validate'):
        assert vitamin_calculator_page.get_results_by_alcohol() == 0
