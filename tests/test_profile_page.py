from os import listdir
from os.path import join, dirname
from random import choice

import allure

from pages.profile_page import ProfilePage
from tests.test_data import data


@allure.feature('Check_profile_page')
@allure.story('Check_modify_profile_form')
def test_modify_profile_form(driver):
    with allure.step('Open_main_page'):
        profile_page = ProfilePage(driver)
        profile_page.open()
    with allure.step('Sign_In'):
        profile_page.sign_in(data.valid_login, data.valid_password)
    with allure.step('Move_to_profile_page'):
        profile_page.click_to_profile_form()
    with allure.step('Getting_auxiliary_data'):
        before_update = profile_page.get_some_profile_items()
        profile_page.update_some_profile_items()
        after_update = profile_page.get_some_profile_items()
    with allure.step('Validate'):
        assert before_update != after_update


@allure.feature('Check_profile_page')
@allure.story('Check_add_avatar_in_profile_form')
def test_add_avatar_with_valid_siz(driver):
    with allure.step('Open_main_page'):
        profile_page = ProfilePage(driver)
        profile_page.open()
    with allure.step('Sign_In'):
        profile_page.sign_in(data.valid_login, data.valid_password)
    with allure.step('Move_to_profile_page'):
        profile_page.click_to_profile_form()
    with allure.step('Getting_auxiliary_data'):
        random_file = choice(listdir(join(join(dirname(__file__)), 'test_data', 'valid_avatars')))
        random_file_with_path = join(join(dirname(__file__)), 'test_data', 'valid_avatars', random_file)
        profile_page.add_avatar(random_file_with_path)
        profile_page.submit_changes()
    with allure.step('Validate'):
        assert profile_page.is_avatar_posted()


@allure.feature('Check_profile_page')
@allure.story('Check_add_avatar_more_than_valid_size')
def test_add_avatar_more_than_valid_size(driver):
    with allure.step('Open_main_page'):
        profile_page = ProfilePage(driver)
        profile_page.open()
    with allure.step('Sign_In'):
        profile_page.sign_in(data.valid_login, data.valid_password)
    with allure.step('Move_to_profile_page'):
        profile_page.click_to_profile_form()
    with allure.step('Getting_auxiliary_data'):
        random_file = choice(listdir(join(join(dirname(__file__)), 'test_data', 'invalid_avatars')))
        random_file_with_path = join(join(dirname(__file__)), 'test_data', 'invalid_avatars', random_file)
        profile_page.add_avatar(random_file_with_path)
    with allure.step('Validate'):
        assert profile_page.get_notify_message() == 'Максимальный размер 4 MB'
