import allure

from pages.water_calculator_page import WaterCalculatorPage


@allure.feature('Check_water_calculator_page')
@allure.story('Check_water_calculator_return_numeric')
def test_water_calculator_return_numeric(driver):
    with allure.step('Open_main_page'):
        water_calculator_page = WaterCalculatorPage(driver)
        water_calculator_page.open()
    with allure.step('Move_to_water_calculator_page'):
        water_calculator_page.move_to_water_calculation_page()
    with allure.step('Getting_auxiliary_data'):
        water_calculator_page.click_start_calculation()
        water_calculator_page.add_sex_in_form()
        water_calculator_page.add_activity_in_form('Без физ.нагрузки (сидячая работа)')
        water_calculator_page.add_calories_in_form(2000)
        water_calculator_page.add_age_in_form(40)
        water_calculator_page.add_weight_in_form(85)
        water_calculator_page.click_calculate()
        result = water_calculator_page.get_mean_result()
        assert isinstance(float(result), (float, int))


@allure.feature('Check_water_calculator_page')
@allure.story('Check_water_calculator_recalculate_with_add_activity')
def test_water_calculator_recalculate_with_add_activity(driver):
    with allure.step('Open_main_page'):
        water_calculator_page = WaterCalculatorPage(driver)
        water_calculator_page.open()
    with allure.step('Move_to_water_calculator_page'):
        water_calculator_page.move_to_water_calculation_page()
    with allure.step('Getting_auxiliary_data'):
        water_calculator_page.click_start_calculation()
        water_calculator_page.add_sex_in_form()
        water_calculator_page.add_activity_in_form('Без физ.нагрузки (сидячая работа)')
        water_calculator_page.add_calories_in_form(2000)
        water_calculator_page.add_age_in_form(40)
        water_calculator_page.add_weight_in_form(85)
        water_calculator_page.click_calculate()
        result = water_calculator_page.get_mean_result()
        water_calculator_page.activity_up_by_sport()
        water_calculator_page.click_recalculate()
        new_result = water_calculator_page.get_mean_result()
    with allure.step('Validate'):
        assert float(new_result) > float(result)


@allure.feature('Check_water_calculator_page')
@allure.story('Check_water_calculator_calculate_with_more_info')
def test_water_calculator_calculate_with_more_info(driver):
    with allure.step('Open_main_page'):
        water_calculator_page = WaterCalculatorPage(driver)
        water_calculator_page.open()
    with allure.step('Move_to_water_calculator_page'):
        water_calculator_page.move_to_water_calculation_page()
    with allure.step('Getting_auxiliary_data'):
        water_calculator_page.click_start_calculation()
        water_calculator_page.add_sex_in_form()
        water_calculator_page.add_activity_in_form('Без физ.нагрузки (сидячая работа)')
        water_calculator_page.add_calories_in_form(2500)
        water_calculator_page.add_age_in_form(25)
        water_calculator_page.add_weight_in_form(70)
        water_calculator_page.click_calculate()
        min_cups = water_calculator_page.get_min_cups_from_graph()
        max_cups = water_calculator_page.get_max_cups_from_graph()
    with allure.step('Validate'):
        assert float(max_cups) > float(min_cups)
