import allure

from pages.main_page import MainPage
from tests.test_data import data


@allure.feature('Check_main_page')
@allure.story('Check_search_working')
def test_search_working(driver):
    with allure.step('Open_main_page'):
        main_page = MainPage(driver)
        main_page.open()
    with allure.step('Getting_auxiliary_data'):
        main_page.input_to_search(data.searching_word)
        main_page.click_to_searching()
        result = main_page.get_searching_result()
    with allure.step('Validate'):
        assert result and len(result) == 10


@allure.feature('Check_main_page')
@allure.story('Check_compare_social_network_ico_by_count')
def test_compare_social_network_ico_by_count(driver):
    with allure.step('Open_main_page'):
        main_page = MainPage(driver)
        main_page.open()
    with allure.step('Getting_auxiliary_data'):
        header_ico_count = len(main_page.get_header_soc_ico())
        footer_ico_count = len(main_page.get_footer_soc_ico())
    with allure.step('Validate'):
        assert header_ico_count == footer_ico_count


@allure.feature('Check_main_page')
@allure.story('Check_compare_social_network_ico_by_link')
def test_compare_social_network_ico_by_link(driver):
    with allure.step('Open_main_page'):
        main_page = MainPage(driver)
        main_page.open()
    with allure.step('Getting_auxiliary_data'):
        header_ico_link = main_page.header_ico_link_attr()
        footer_ico_link = main_page.footer_ico_link_attr()
    with allure.step('Validate'):
        assert header_ico_link == footer_ico_link


@allure.feature('Check_main_page')
@allure.story('Check_that_notification_not_empty')
def test_notification_not_empty(driver):
    with allure.step('Open_main_page'):
        main_page = MainPage(driver)
        main_page.open()
    with allure.step('Getting_auxiliary_data'):
        main_page.click_to_notification()
        main_page.click_to_notification_more()
        notification_content = main_page.get_notification_content()
    with allure.step('Validate'):
        assert len(notification_content) > 0


@allure.feature('Check_main_page')
@allure.story('Check_that_notification_show_more_working')
def test_notification_show_more_working(driver):
    with allure.step('Open_main_page'):
        main_page = MainPage(driver)
        main_page.open()
    with allure.step('Getting_auxiliary_data'):
        main_page.click_to_notification()
        before_more = main_page.get_notification_content()
        main_page.click_to_notification_more()
        after_more = main_page.get_notification_content()
    with allure.step('Validate'):
        assert len(before_more) != len(after_more)
