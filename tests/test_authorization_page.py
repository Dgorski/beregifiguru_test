import allure
import pytest

from pages.authorization_page import AuthorizationPage
from tests.test_data import data


@allure.feature('Check_authorization')
@allure.story('Signin_with_invalid_credentials')
@pytest.mark.parametrize(
    'login, password',
    [
        ('', ''),
        ('qap10@gmail.com', ''),
        ('', 'qwerty1!'),
        ('qap10@gmail.com', 'qwerty1!')
    ]
)
def test_not_possible_signin_with_invalid_credentials(driver, login, password):
    with allure.step('Open_main_page'):
        auth_page = AuthorizationPage(driver)
        auth_page.open()
    with allure.step('Sign_In'):
        auth_page.sign_in(login, password)
    with allure.step('Validate'):
        assert auth_page.error_message().is_enabled()


@allure.feature('Check_authorization')
@allure.story('Signin_with_valid_credentials')
def test_possible_signin_with_valid_credentials(driver):
    with allure.step('Open_main_page'):
        auth_page = AuthorizationPage(driver)
        auth_page.open()
    with allure.step('Sign_In'):
        auth_page.sign_in(data.valid_login, data.valid_password)
    with allure.step('Validate'):
        assert data.valid_login.split('@')[0] in auth_page.personal_diary_txt()


@allure.feature('Check_authorization')
@allure.story('Signup_after_signin')
def test_signup_working_after_authorisation(driver):
    with allure.step('Open_main_page'):
        auth_page = AuthorizationPage(driver)
        auth_page.open()
    with allure.step('Sign_In'):
        auth_page.sign_in(data.valid_login, data.valid_password)
    with allure.step('Sign_Out'):
        auth_page.sign_out()
    with allure.step('Validate'):
        assert auth_page.signin_signup_status_txt() == 'Войти'


@allure.feature('Check_authorization')
@allure.story('If_cookie_not_store_my_login/pass')
def test_if_cookie_not_store_personal_data(driver):
    with allure.step('Open_main_page'):
        auth_page = AuthorizationPage(driver)
        auth_page.open()
    with allure.step('Sign_In'):
        auth_page.sign_in(data.valid_login, data.valid_password)
    with allure.step('Validate'):
        assert data.valid_login or data.valid_password not in auth_page.get_cookies()
