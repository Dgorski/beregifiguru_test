import allure

from pages.calories_consumption_page import CaloriesConsumptionPage


@allure.feature('Check_calories_calculator_page')
@allure.story('Check_less_weight_less_calories_burned')
def test_less_weight_less_calories_burned(driver):
    with allure.step('Open_main_page'):
        calories_consumption_page = CaloriesConsumptionPage(driver)
        calories_consumption_page.open()
    with allure.step('Move_to_calculator'):
        calories_consumption_page.move_to_calories_consumption_page()
    with allure.step('Getting_auxiliary_data'):
        calories_consumption_page.click_to_aerobic_fitness()
        calories_consumption_page.add_weight_in_form(85)
        before = calories_consumption_page.get_calories_consumption_for_hiit()
        calories_consumption_page.add_weight_in_form(75)
        after = calories_consumption_page.get_calories_consumption_for_hiit()
    with allure.step('Validate'):
        assert float(before) > float(after)


@allure.feature('Check_calories_calculator_page')
@allure.story('Check_more_time_more_calories_burned')
def test_more_time_more_calories_burned(driver):
    with allure.step('Open_main_page'):
        calories_consumption_page = CaloriesConsumptionPage(driver)
        calories_consumption_page.open()
    with allure.step('Move_to_calculator'):
        calories_consumption_page.move_to_calories_consumption_page()
    with allure.step('Fill_the_form'):
        calories_consumption_page.click_to_aerobic_fitness()
        calories_consumption_page.add_weight_in_form(85)
        calories_consumption_page.add_time_in_form(45)
        calories_consumption_page.clik_to_hiit_training_info()
        times = calories_consumption_page.get_times_from_hiit_table_info()
        calories = calories_consumption_page.get_calories_from_hiit_table_info()
        result = list(zip(times, calories))
    with allure.step('Validate'):
        assert all((result[i][0] < result[i + 1][0] and result[i][1] < result[i + 1][1]) for i in range(len(result) - 1))
